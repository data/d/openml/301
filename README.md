# OpenML dataset: ozone_level

https://www.openml.org/d/301

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown -   
**Please cite**:   

1. Title: Ozone Level Detection
2. Source:
Kun Zhang
zhang.kun05 '@' gmail.com
Department of Computer Science, 
Xavier University of Lousiana

Wei Fan
wei.fan '@' gmail.com
IBM T.J.Watson Research

XiaoJing Yuan
xyuan '@' uh.edu
Engineering Technology Department, 
College of Technology, University of Houston 

3. Past Usage:
Forecasting skewed biased stochastic ozone days: analyses, solutions and beyond, Knowledge and Information Systems, Vol. 14, No. 3, 2008.
Discusses details about the dataset, its use as well as various experiments (both cross-validation and streaming) using many state-of-the-art methods.

A shorter version of the paper (does not contain some detailed experiments as the journal paper above) is in:
Forecasting Skewed Biased Stochastic Ozone Days: Analyses and Solutions. ICDM 2006: 753-764 

4. Relevant Information:
The following are specifications for several most important attributes 
that are highly valued by Texas Commission on Environmental Quality (TCEQ). 
More details can be found in the two relevant papers.
 
-- O 3 - Local ozone peak prediction
-- Upwind - Upwind ozone background level
-- EmFactor - Precursor emissions related factor
-- Tmax - Maximum temperature in degrees F
-- Tb - Base temperature where net ozone production begins (50 F)
-- SRd - Solar radiation total for the day
-- WSa - Wind speed near sunrise (using 09-12 UTC forecast mode)
-- WSp - Wind speed mid-day (using 15-21 UTC forecast mode) 

5. Number of Instances: 2536

6. Number of Attributes: 73

7. Attribute Information:
1,0 | two classes 1: ozone day, 0: normal day

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/301) of an [OpenML dataset](https://www.openml.org/d/301). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/301/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/301/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/301/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

